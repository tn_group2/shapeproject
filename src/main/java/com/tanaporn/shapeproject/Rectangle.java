/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.shapeproject;

/**
 *
 * @author HP
 */
public class Rectangle {
    private double a,b;
    public Rectangle(double a,double b) {
        this.a = a;
        this.b = b;
    }
    public double calArea() {
        return a*b;
    }
    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
    public void setA(double a) {
        if(a <= 0) {
            System.out.println("Error: hight must more than zero!!!");
            return ;
        }
        this.a = a;
    }
    public void setB(double b) {
        if(b <= 0) {
            System.out.println("Error: wide must more than zero!!!");
            return ;
        }
        this.b = b;
    }
}
